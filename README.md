# fyp-myappointment

- Module: CSIT 321 - Project
- Group Number: FYP-21-S2-24

# Group Member:

- Ling Yan Ying  (Team Leader | Back-End Developer | Software Tester)
- Kadek Anantawijaya Swadharma (Lead Developer | Software Designer)
- Ho Murn Por Jeremy (Software Analyst | Marketing Site Moderator)
- Lee Kheen Hong  (Main Software Designer | Software Analyst)
- Kshitij Bir Arora (Software Analyst | Front-End Developer)
- Lim Jin Hin (Developer | Software Tester)


# Reminder
- composer update  

# Folders & Files Explanation:
- '.htaccess' is to restrict access to certain folders.


